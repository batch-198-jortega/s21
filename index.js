let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/

function addUsers(user) {

    users[users.length] = user;
    console.log(users);

}

addUsers("John Cena");


/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

function findItem(index) {

    return users[index];

}

let itemFound = findItem(2);

console.log(itemFound);


/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/

function storeLastItem() {

    let lastItem = users[users.length-1];
    users.length--;
    return lastItem;

}

let lastUser = storeLastItem();
console.log(lastUser);
console.log(users);



/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/

function updateUsers(index,newUser) {

    users[index] = newUser;

}

updateUsers(3,'Triple H');
console.log(users);



/*
    5. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/

function checkEmptyArray() {

    if (users.length > 0) {
        return false;
    } else {
        return true;
    }

}

let isUsersEmpty = checkEmptyArray();
console.log(isUsersEmpty);

/*
    Stretch Goal

    Create a function which is able to delete all items in the array.
*/

function modifyArray(length) {

    if(users.length <= length) {

        for(let index = users.length; index <= length; index++) {

            if (index < length) {
                let addUser = prompt("Add user: ");
                users[users.length] = addUser;
            }

        }

    } else if (users.length > length) {

        for(let index = users.length; index > length; index--) {

            users.length--;

      }

    }

    console.log(users);

}

modifyArray(0);

